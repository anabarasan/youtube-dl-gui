#! /usr/bin/env python3
#  -*- coding: utf-8 -*-
#

from tkinter import *
from tkinter import messagebox
import traceback
import threading

import youtube_dl


class YoutubedlGUI:
    def __init__(self, root=None):
        """This class configures and populates the toplevel window.
           root is the toplevel containing window."""

        self.video_url = StringVar()

        root.geometry("600x187+595+199")
        root.title("Youtube Downloader")
        root.configure(highlightcolor="black")

        self.top_level = root

        self.Frame1 = Frame(root)
        self.Frame1.place(relx=0.02, rely=0.05, relheight=0.88, relwidth=0.98)
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(width=585)

        self.Label1 = Label(self.Frame1)
        self.Label1.place(relx=0.02, rely=0.06, height=18, width=566)
        self.Label1.configure(activebackground="#f9f9f9")
        self.Label1.configure(anchor=W)
        self.Label1.configure(text='''URL''')

        self.Entry1 = Entry(self.Frame1)
        self.Entry1.place(relx=0.02, rely=0.24, height=30, relwidth=0.95)
        self.Entry1.configure(background="white")
        self.Entry1.configure(font="TkFixedFont")
        self.Entry1.configure(selectbackground="#c4c4c4")
        self.Entry1.configure(textvariable=self.video_url)

        self.Button1 = Button(self.Frame1)
        self.Button1.place(relx=0.1, rely=0.55, height=46, width=97)
        self.Button1.configure(activebackground="#d9d9d9")
        self.Button1.configure(command=self.download)
        self.Button1.configure(text='''Download''')

        self.Button2 = Button(self.Frame1)
        self.Button2.place(relx=0.41, rely=0.55, height=46, width=116)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(command=self.clear_url)
        self.Button2.configure(text='''Clear''')
        self.Button2.configure(width=116)

        self.Button3 = Button(self.Frame1)
        self.Button3.place(relx=0.72, rely=0.55, height=46, width=117)
        self.Button3.configure(activebackground="#d9d9d9")
        self.Button3.configure(command=self.destroy_window)
        self.Button3.configure(text='''Quit''')
        self.Button3.configure(width=117)

    def clear_url(self):
        self.video_url.set("")

    def destroy_window(self):
        self.top_level.destroy()
        self.top_level = None

    def download(self):
        threading.Thread(target=self._download).start()

    def _download(self):
        options = {
            'format': 'bestaudio/best',                 # choice of quality
            'extractaudio' : True,                      # only keep the audio
            'audioformat' : "mp3",                      # convert to mp3
            'outtmpl': '%(title)s.%(ext)s',      # name the file the name of the video
            'noplaylist': True,                         # only download single song, not playlist
        }
        ydl = youtube_dl.YoutubeDL(options)
        try:
            result = ydl.extract_info(self.Entry1.get(), download=True)
            print(result['id'], result['title'])
            self.clear_url()
            messagebox.showinfo("Download", "Download complete.")
        except Exception as exn:
            print("Can't download! %s\n" % traceback.format_exc())
            print(exn)


def main():
    root = Tk()
    YoutubedlGUI(root)
    root.mainloop()


if __name__ == '__main__':
    main()
